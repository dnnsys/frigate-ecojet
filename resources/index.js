// APPLICATION
import './assets/stylesheets/sass/application.sass'
import './assets/javascripts/Application';


// PAGES
import './views/pages/index.njk'
import './views/pages/404.njk'
import './views/pages/500.njk'

import './views/pages/company/team.njk'

import './views/pages/press-center/news.njk'
import './views/pages/press-center/news-single.njk'
import './views/pages/press-center/documents.njk'
import './views/pages/press-center/gallery/photo-gallery.njk'
import './views/pages/press-center/gallery/video-gallery.njk'
import './views/pages/press-center/gallery/logo-gallery.njk'

import './views/pages/programm/target.njk'
import './views/pages/programm/status.njk'

import './views/pages/airplane/frigate-ecojet.njk'
import './views/pages/airplane/freejet.njk'
import './views/pages/airplane/composition.njk'
import './views/pages/airplane/advantages.njk'