window.$ = window.jQuery = require('jquery');
require('slick-carousel')

export default class AdvantagesSlider{
  constructor(){
    this._init()
  }

  _init(){
    let $sliderContainer = $('.advantages-carousel')
    let prevArrow = $sliderContainer.siblings('.advantages-carousel__nav').find('.advantages-carousel__nav-arrow--prev')
    let nextArrow = $sliderContainer.siblings('.advantages-carousel__nav').find('.advantages-carousel__nav-arrow--next')

    $sliderContainer.slick({
      adaptiveHeight: true,
      infinite: false,
      prevArrow: prevArrow,
      nextArrow: nextArrow
    })
  }
}
