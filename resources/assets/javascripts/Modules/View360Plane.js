window.$ = window.jQuery = require('jquery');
require("spritespin")
require('ion-rangeslider')

export default class View360Plane{
  constructor(){
    this._init()
    //this._rangeSliderFor360View()
  }

  _init(){
    let $container = $('#container360view')
    let srcImg = $container.data('src')
    let $rangeInput = $("#example_id")

    if($container.length){
      $container.spritespin({
        // path to the source images.
        source: [
          srcImg,
        ],
        //width   : 480,  // width in pixels of the window/frame
        //height  : 327,  // height in pixels of the window/frame
        frames: 24,
        framesX: 1,
        sense: -1,
        responsive: true,
        height: 500,
        animate: false,
        frameTime: 100,
        loop: false,
        //stopFrame: 25,
        wrap: false,
        onInit: () => {
          this._rangeSliderFor360View($rangeInput, $container)
        },
        onFrameChanged: (data, frame, lane) => {
          let frameNumber = (frame.frame).toString()
          console.log(frameNumber)
          this._rangeChange($rangeInput, frameNumber)
        }
      });
    }
  }

  _rangeSliderFor360View(range, container360){
    range.ionRangeSlider({
      min: 0,
      max: 23,
      onChange: function (data) {
        console.log(data.from)
        container360.spritespin({
          frame: data.from
        })
      }
    });
  }

  _rangeChange(range, from){
    range.data('ionRangeSlider').update({
      from: from
    });
  }
}