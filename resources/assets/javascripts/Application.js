//VENDORS
window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
require('magnific-popup')
require('jquery-ui')
import lightbox from 'lightbox2'
import Parallax from 'parallax-js'

//import custom classes
import View360Plane from './Modules/View360Plane'
import AdvantagesSlider from './Modules/AdvantagesSlider'

class Application{
    constructor(){
        this._init()
    }

    _init(){
        //private methods
        this._mainPageParallax()
        this._mainPageChangeContent()
        this._scriptForSvg()
        this._burgerMenu()
        this._switcherSelect()
        this._statusSchemeClick()
        this._freeJet360ViewButton()
        this._initPopup()
        this._hoverBulletAnimation()
        this._teamItemOpenModal()
        this._timeLineTabs()
        this._mainMenuAccordion()
        this._photoGalleryLightbox()

        //classes
        new View360Plane()
        new AdvantagesSlider()
    }

    _mainPageParallax(){
      let $parallaxScene = $('#parallax-scene')
      if($parallaxScene.length){
        let scene = $('#parallax-scene').get(0);
        let parallaxInstance = new Parallax(scene);
      }
    }

    _mainPageChangeContent(){
      let $mainBlock2 = $('#main-block-2')

      $('#main-page-more').on('click', () => {
        $('#parallax-scene').fadeOut()
        setTimeout(function () {
          $('#parallax-clouds').fadeOut()
        }, 500)
        $('#main-block-1').fadeOut()
        $mainBlock2.addClass('show')
      })
    }

    _burgerMenu(){
        $('.js-burger-menu').on('click', (e) => {
          let $this = $(e.currentTarget)
          let $header = $('.header')

          $this.toggleClass('opened')
          $('body').toggleClass('menu-opened')

          //clearTimeout(slideHeader)
          this._slideMenu()
        })
    }

    _slideMenu(){
        let $header = $('header')
        let headerHeight = $header.outerHeight()

        $('#main-menu').toggleClass('opened')
        $header.toggleClass('menu-opened')

        $('.main-menu__inner').css('margin-top', (headerHeight + 10) + 'px')

        if($header.hasClass('menu-opened') && !$('main').hasClass('main-page')){
          $('main').css('margin-top', headerHeight)
        }else{
          $('main').css('margin-top', '0')
        }
    }

    _switcherSelect(){
      let $switcher = $('.switcher')
      let $switcherInput = $switcher.find('input')

      this._switchContent($switcher, $switcherInput)

      $switcherInput.on('change', (e) => {
        let $this = $(e.currentTarget)
        $this.parent().find('.switcher__value').toggleClass('switcher__value--active')

        this._switchContent($switcher, $this)
      })
    }

    _switchContent(switcher){
      let $switchContentContainer = $('.js-switch-container')
      let $switchContent = $switchContentContainer.find('.js-switch-container__content')
      let switchValue = switcher.find('.switcher__value--active').data('target')

      $switchContent.removeClass('show')

      $switchContentContainer.find('#' + switchValue).addClass('show')
    }

    _scriptForSvg(){
      $('img.svg').each(function(){
        let $img = $(this);
        let imgID = $img.attr('id');
        let imgClass = $img.attr('class');
        let imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          let $svg = $(data).find('svg');

          // Add replaced image ID to the new SVG
          if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
          }
          // Add replaced image classes to the new SVG
          if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

          // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
          if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
          }

          // Replace image with new SVG
          $img.replaceWith($svg);

        }, 'xml');

      });
    }

    _freeJet360ViewButton(){
      $('.js-freejet-360-button').on('click', (e) => {
        let $this = $(e.currentTarget)

        $this.toggleClass('active')

        $('.plane-scheme').toggle()

        $('.view-360-block').toggleClass('show')
      })
    }

    _statusSchemeClick(){
      $('.status-scheme__item').on('click', (e) => {
        let $this = $(e.currentTarget)

        let elemDataAttr = $this.data('target')

        $('.status-scheme__item').removeClass('active')

        $this.addClass('active')


        $('.status-scheme-dropdown').removeClass('show')

        $('#status-scheme-dropdown-' + elemDataAttr).addClass('show')
      })
    }

    _initPopup(){
      $('.popup-modal-btn').magnificPopup({
        type: 'inline',
        mainClass: 'my-mfp-zoom-in'
      });
    }
    
    _hoverBulletAnimation(){
      let $hoverBullet = $('.hover-bullet')
      let $bulletContainer = $hoverBullet.parents('div')

      $hoverBullet.on('mouseenter', (e) => {
        let $this = $(e.currentTarget)
        let descrBlock = $this.find('.hover-bullet__description')
        let desBlockHeight = descrBlock.height()
        let desBlockWidth = descrBlock.width()


        let line = $("<div class='hover-bullet__line'></div>")

        let circlePos = line.position()
        let descrBlockPos = descrBlock.position();

        let x1 = circlePos.left,
            y1 = circlePos.top,
            x2 = descrBlockPos.left - 4,
            y2 = descrBlockPos.top + desBlockHeight;

        if($this.hasClass('hover-bullet--left')){
          x2 = descrBlockPos.left + desBlockWidth - 5
        }

        console.log(x1, y1, x2, y2)

        let w = $this.height() / 2

        line.css({
          top: y1 + w,
          left: x1 + w,
          transform: 'rotate('+Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI+'deg)'
        }).animate({width: Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))})

        $this.addClass('show')

        $this.append(line)

      }).on('mouseleave',  (e) => {
        let $this = $(e.currentTarget)

        $this.find('.hover-bullet__line').animate({width: '0'}, 500)
        let timeOut = setTimeout((e) => {
          $this.find('.hover-bullet__line').remove()
        }, 500)
        clearTimeout(timeOut)
        $this.removeClass('show')
      })
    }

    _teamItemOpenModal(){
      $('.popup-team-modal-btn').on('click', (e) => {
        let idModal = $(e.currentTarget).data('id')
        console.log(idModal)

        $.magnificPopup.open({
          items: {
            src: '#'+idModal
          },
          type: 'inline'
        });
      })
    }

    _timeLineTabs(){
      let $tabs = $('.time-line-tabs')

      $('.time-line-menu__item').on('click', (e) => {
        let $this = $(e.currentTarget)
        let $tabsContent = $this.closest($tabs).find('.time-line-tabs__content')
        let $tabsLink = $this.closest($tabs).find('.time-line-menu__item')
        let dataId = $this.data('id')

        $tabsContent.removeClass('active')
        $tabsLink.removeClass('active')

        $this.addClass('active')
        $this.closest($tabs).find('div[data-tab="' + dataId + '"]').addClass('active')
      })
    }

    _mainMenuAccordion(){
      if($(window).width() < 576){
        $('.main-menu__section-title').on('click', function () {
          let $this = $(this)
          let $menuList = $this.siblings('.main-menu__list-items')
          if($menuList.length){
            $menuList.slideToggle()
          }
        })
      }
    }

    _photoGalleryLightbox(){
      lightbox.option({
        disableScrolling: true,
        albumLabel: 'Изображение %1 из %2'
      })
    }
}

new Application();
